
$(document).ready(function(){
    $('.navigation-depth-01').each(function(){
        if ( $(this).is('[data-togger]') ) {
            $(this).find('a').click(function() {
                $(this).next('.navigation-depth-02').toggle();
                $(this).parent().siblings().find('.navigation-depth-02').hide();
                return false;
            });
        }
        return;
    })
})